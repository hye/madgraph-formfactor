from MadGraphControl.MadGraphUtils import *
import re
import subprocess
import sys
import shutil

nevents = 1.2*runArgs.maxEvents

# Set beam energy
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

# Write process card
with open("proc_card_mg5.dat", 'w') as fname:
    fname.write('import model ./Pseudoscalar_2HDM_FormFactor\n')
    fname.write('''
    define p = g u c d s u~ c~ d~ s~ b b~ 
    define j = p
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    ''')
    fname.write('generate g g > {0} > t t~ QED=99 QCD=99, ( t > b w+, w+ > l+ vl), ( t~ > b~ w-, w- > j j) @1\n'.format(mediator))
    fname.write('add process g g > {0} > t t~ QED=99 QCD=99, ( t > b w+, w+ > j j), ( t~ > b~ w-, w- > l- vl~) @2\n'.format(mediator))
    fname.write('add process g g > {0} > t t~ QED=99 QCD=99, ( t > b w+, w+ > l+ vl), ( t~ > b~ w-, w- > l- vl~) @3\n'.format(mediator))
    fname.write('add process g g > {0} > t t~ QED=99 QCD=99, ( t > b w+, w+ > j j), ( t~ > b~ w-, w- > j j) @4\n'.format(mediator))
    fname.write('output -f\n')
    #fname.write('launch\n')
fname.close()

process_dir = new_process(card_loc='proc_card_mg5.dat')

# Copy parameter card into main directory
shutil.copy(process_dir+'/Cards/param_card.dat', 'param_card.dat')

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0',
           'cut_decays':'F',
           'pdlabel':"'nn23lo1'",
           'use_syst':"False",
           'pdfwgt':'F'}

try:
    os.remove('run_card.dat')
except OSError:
    pass


build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
runName='run_01'

# Generation of events
generate(param_card_loc='./param_card.dat',run_card_loc='./run_card.dat',proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',saveProcDir=True)

### Shower 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


evgenConfig.description = 'MadGraph_2HDM_FormFactor'
evgenConfig.inputfilecheck = runName
evgenConfig.keywords = ["BSM", "BSM_top"]
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.contact  = ['']
runArgs.inputGeneratorFile = runName + '._00001.events.tar.gz'
