#!/bin/bash

targetPath="PROC_Pseudoscalar_2HDM_FormFactor_0"

if [ -d "$targetPath" ]; then 
	rm -rf $targetPath
        echo 'Delete PROC_Pseudoscalar_2HDM_FormFactor_0/ folder.'
fi


Gen_tf.py --ecmEnergy=13000. --maxEvents=10 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=tmp.EVNT.root --jobConfig=./
