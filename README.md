MadGraph JobOptions for A/H to ttbar
======================================
Set up the environment
--------------------------
```bash
setupATLAS 
asetup 21.6.21,AthGeneration,here  
export PYTHONPATH=/srv/:$PYTHONPATH
```
Run the generation
--------------------------
```bash
source run_gen.sh
```
